# Modifier bootstrap

Règle : Ne jamais modifier des fichiers vendors !

Règle : Mettre le fichier css après celui de bootstrap pour que les changements soit pris en compte.

## Modifier le bootstrap en ajoutant une classe au container

1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple
2. Faire un import de ce fichier dans le fichier styles.css
3. Importer aussi le styles.css dans le fichier html
4. Rajouter une classe my-container au container
5. Apporter les modifications voulu sur le fichier mod.bootstrap.css a l'aide de la classe créé

## Modifier le bootstrap directement dans le container
1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple
2. Faire un import de ce fichier dans le fichier styles.css
3. Importer aussi le styles.css dans le fichier html
4. Modifier .container sans créer la classe my-container
5. Apporter les modifications voulu sur le fichier mod.bootstrap.css a l'aide de .container directement
# Les Containers

- [container & container fluid](#container-container-fluid)
    - [container](#container-le-contenu-est-centré-au-milieu-de-la-page)
    - [container-fluid](#container-fluid-le-contenu-sétend-sur-la-largeur)
- [row & col](#row-col)
    - [taille des col](#taille-des-col)

## Container & container fluid

#### Container : Le contenu est centré au milieu de la page

````html
<div class="container">
    <div>
        ... contenu ...
    </div>
</div>
````
#### Container-fluid : Le contenu s'étend sur la largeur

````html
<div class="container-fluid">
    <div>
        ... contenu ...
    </div>
</div>
````
 
## Row & col

Généralement, une row est composée de col, l'un ne va pas sans l'autre

````html
<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
````
On peut créer des row dans des cols mais il faut toujours commencé par row et finir par col, même vide. Une row peut contenir plusieurs col.

````html
<div class="container">
    <div class="row">
        <div class="col"></div>
            <div class="row">
                <div class="col"></div>
                <div class="col"></div>
            </div>
        <div class="col"></div>
    </div>
</div>
````
#### Taille des col
Il y'a de la place pour 12 colonnes, ou moins selon leur taille. Elles doivent être réparties en fonction de la taille qu'on veut leurs donner.

````html
<div class="col-4"></div>
<div class="col-2"></div>
<div class="col-4"></div>
<div class="col-2"></div>
````

Un offset laissera un espace non utilisé sur la gauche d'une colonne et décalera donc le contenu vers la droite.

````html
<div class="col-5 offset-1"></div>
<div class="col-6"></div>
````

A l'inverse, une espace non utilisé sans même un offset, laissera un vide a droite.

````html
<div class="col-5 offset-1"></div>
<div class="col-5"></div>
````
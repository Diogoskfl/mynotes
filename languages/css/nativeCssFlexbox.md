# Css natif : flexbox
On dit css natif quand celui ci ne se repose pas sur un framework comme [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction), tout est fait et creer par le developpeur.

- **Table des matières :**
    - [flex direction](#flex-direction)
    - [flex wrap](#flex-wrap)
    - [justify content](#justify-content)
    - [align items](#align-items)
    - [align content](#align-content)
    - [flex](#flex)

## display, flex,inline-flex

C'est ainsi qu'on définit un container flex, il est block par défaut ou inline selon la valeur donnée. Cela cée un contexte flex pour tous les descendants directs.

* ```flex``` : Cette valeur génère un container flex, de niveau block, à l'intérieur de l'élément.
* ```inline-flex``` : Cette valeur génère un container flex, de niveau inline, à l'intérieur de l'élément.

## flex direction

La propriété flex-direction établit l'axe principal.

``` css
flex-direction: row | row-reverse | column | column-reverse
```
La propriété flex-direction établit l'axe principal.

* ```row``` : de gauche à droite
* ```column``` : comme row mais du haut vers le bas

Le mot-clé ` reverse ` peut être utilisé pour changer le sens de ceux-ci.

## flex wrap

Cette propriété définit si le container comprend une seule ligne ou plusieurs et la direction de celles-ci.

```css
flex-wrap: nowrap | wrap | wrap-reverse
```

* ``` nowrap ``` : (valeur par défaut) sur une seule ligne, de gauche à droite dans un système ltr, sinon l'inverse. La ligne peut déborder de son contenant.
* ``` wrap ``` : multiligne, de gauche à droite dans un système ltr, sinon l'inverse. Pas de débordement, on passe à la ligne.
* ``` wrap-reverse ``` : multiligne, de droite à gauche dans un système ltr, sinon l'inverse.

## justify content

La propriété justify-content définit l'alignement le long de l'axe principal. Elle permet de distribuer l'espace excédentaire lorsque tous les items flex sur une ligne sont inflexibles ou lorsqu'ils ont atteint leur taille maximale. Elle contrôle aussi l'alignement des items lorsqu'ils débordent.
``` css
justify-content: flex-start | flex-end | center | space-between | space-around
```
-   ``` flex-start ``` (par défaut) : les items sont regroupés en début de ligne
-   ``` flex-end ``` : les items sont regroupés en fin de ligne
-   ``` center ``` : les items sont centrés le long de la ligne
-   ``` space-between ``` : les items sont répartis sur la ligne; le premier est collé du côté start, le dernier du côté end.
-   ``` space-around ``` : les items sont répartis sur la ligne avec un espacement égal autour de chacun.

## align items

La propriété align-items définit la façon dont les items d'une ligne sont disposés le long de l'axe "cross".
``` css
align-items: flex-start | flex-end | center | baseline | stretch
```

* ``` flex-start ``` : l'item est placé au début de la ligne cross-start.
* ``` flex-end ``` : la marge "cross-end" de l'item est placée sur la ligne cross-end
* ``` center ``` : les items sont centrés sur l'axe cross
* ``` baseline ``` : les items sont alignés sur leur ligne de base
* ``` stretch ``` (par défaut) : les items sont étirés jusqu'à remplir le container (tout en respectant min-width/max-width)

## align content

La propriété align-content aligne les lignes d'un container flex à l'intérieur de l'espace où il reste de l'espace, un peu comme justify-content aligne les items sur l'axe principal.
```css
align-content: flex-start | flex-end | center | space-between | space-around | stretch
```
* ``` flex-start ``` : lignes regroupées au début du container
* ``` flex-end ``` : lignes regroupées à la fin du container
* ``` center ``` : lignes regroupées au centre du container
* ``` space-between ``` : les lignes sont réparties, la première est collée du côté start, la dernière du côté end.
* ``` space-around ``` : les lignes sont réparties avec un espacement égal autour de chacune.
* ``` stretch ``` : les lignes s'étirent pour remplir tout l'espace.

## flex

Cette propriété est le raccourci de flex-grow , flex-shrink et flex-basis . Les deuxième et troisième paramètres sont optionels. La valeur par défaut est 0 1 auto.
```css
flex: none | flex-grow | flex-shrink | flex-basis
```
## align self

La propriété align-self permet à des items flex de passer outre aux alignement par défaut ou à ceux spécifiés par align-items. Les valeurs sont les mêmes que pour ce dernier.

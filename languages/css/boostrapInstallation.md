# Installer Bootstrap

## Etapes
1. Créer un nouveau projet
2. Créer les dossiers (assets, main)
3. Ouvrir le terminal et entrer npm init
4. Puis mettre entrée à chaque fois jusqu'au package name
5. Entrer le nom
6. Mettre npm install (ou npm i) bootstrap
7. Une fois le dossier node module crée, on crée le fichier html dans main
8. Relier le fichier bootstrap avec :

````html
<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">

````

## Utiliser le fichier JS
1. Pour utiliser le fichier JS de bootstrap, nous avons besoin de Jquery. Même étapes que pour bootstrap, avec npm i jquery
2. Il ne reste plus qu'a relier les fichier dans le fichier html en prenant soin de mettre jquery en premier :

````html
<script src="../node_modules/jquery/dist/jquery.js">
<script src="../node_modules/bootstrap/dist/js/bootstrap-bundle-min.js">
````
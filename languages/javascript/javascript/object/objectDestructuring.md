# Les objets

## Destructuration des objets

C'est une méthode pour manipuler des keys et values des objects.

### Méthode

````javascript
const user = {
    firstName: 'Valentin',
    lastName: 'Diogo',
    age: 25,
    eyes: 'blue'
};

const age = user.age;
const lastName = user.lastName;

console.log(age, lastName);
````

La console va renvoyer l'age et le nom.

### Destructuration

````javascript
const user = {
    firstName: 'Valentin',
    lastName: 'Diogo',
    age: 25,
};

const {age, lastName} = user;

console.log(age, lastName);
````

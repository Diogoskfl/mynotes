### Vocabulaire

**Typescript** : *TypeScript* est un langage de programmation libre qui a pour but d'améliorer et de sécuriser la production de code JavaScript. TypeScript permet un typage statique optionnel des variables et des fonctions, la création de classes et d'interfaces, l'import de modules, tout en conservant l'approche non-contraignante de JavaScript.

**Truthy/Falsy** : Un élèment est dit *Truthy* s'il existe et renvoie `true` . À l'inverse, il est dit *Falsy* s'il n'existe pas et renvoie `false` . Par exemple : un nombre, un objet ou un string qui n'est pas vide existent et renvoient `true` , ce sont des truthy. Il existent des Falsy particuliers comme le nombre 0, un string vide ou encore l'inverse d'un élèment existant verifié avec `!element` .

**Rest parameter** : Il s'agit d'un outils permettant d'utiliser un nombre de paramètres non défini dans un fonction. Il s'éxprimera avec `...` .

Par exemple : 

````javascript
function example(...args) {
    console.log(args);
}
example(2, 3, 4, 5);

// La console affichera tout les nombres placés en argument
````

### Try/Catch

Le **try/catch** est une mesure de sécurité qui permet de renvoyer un message d'erreur et qui empèche le serveur de planter.

Exemple de Try/Catch : 

````javascript
function myFunction(param) {
    try {
        return ...code...
    } catch (e) {
        return ...erreur... (Par exemple : res.status(500).send({error: 'erreur serveur :' + e.message});)
    }
}
````
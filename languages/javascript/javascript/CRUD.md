## C.R.U.D (Create, Read, Update, Delete)

*CRUD* est un acronyme pour les façons dont on peut fonctionner sur des données stockées. C'est un moyen mémotechnique pour les quatre fonctions de base du stockage persistant, généralement utilisées sur les *bases de données*.

#### create
Fait référence à l'ajout d'un nouvel élement dans la base de données. En javascript et avec le protocole HTTP, nous utiliserons le verbe ` post ` .

#### read
Fait référence à la lecure et/ou récupétation d'un élement dans la base de données. En javascript et avec le protocole HTTP, nous utiliserons le verbe ` get ` .

#### update
Fait référence à la mise à jour/modification d'un élement dans la base de données. En javascript et avec le protocole HTTP, nous utiliserons le verbe ` put ` pour remplacer un élement entièrement et ` patch ` pour modifier seulement une ou plusieurs propriétés d'un élement.

#### delete
Fais référence à la suppression d'un élement dans la base de données. En javascript et avec le protocole HTTP, nous utiliserons aussi le verbe ` delete ` .
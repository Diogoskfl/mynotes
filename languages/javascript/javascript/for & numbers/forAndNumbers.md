- **Table des matières :**
    - [For](#for)
    - [Numbers](#numbers)
        - [Math.round](#mathround)
        - [Math.floor](#mathfloor)
        - [toFixed](#tofixed)
        - [parseInt & parseFloat](#parseint-et-parsefloat)
        - [Math.random](#mathrandom)

## For

````javascript
for (let i = 0, i <= 20, i = i + 1 ) {
    console.log('Toto')
}
````

1. Premier parametre = On initialise i ( let i = .. )
2. Deuxième parametre = La condition de continuation
3. Troisieme parametre = Nouvelle valeur de i

On peut générer des tableaux avec la boucle for.

## Numbers

### Math.round

La fonction Math.round() retourne la valeur d'un nombre arrondi à l'entier le plus proche.

````javascript
console.log(Math.round(10.9));
````

La console va renvoyer 11.

### Math.floor

La fonction Math.floor() renvoie le plus grand entier qui est inférieur au nombre.

````javascript
console.log(Math.floor(10.5);
````

La console va renvoyer 10. Il va garder que ce qu'il y a avant la virgule.

### toFixed

La méthode toFixed() permet de fixé un nombre de décimale après la virgule.

````javascript
console.log(10.66668787878.toFixed(2));
````

La console va renvoyer 10.67.

### parseInt et parseFloat

Les fonctions `parseFloat()` et `parseInt()` permettent de transformer une chaîne de caractères en un nombre flottant ou entier.

### Math.random

Il renvoit un nombre aléatoire compris entre 0 et 1 (le 1 n'est pas compris).

````javascript
console.log(Math.random());
````

La console va renvoyer 0.06123456743.  A partir de ce nombre on va pouvoir fabriquer des nombres aléatoires.

````javascript
console.log(Math.random() * 1000 + 1);
````

La console va renvoyer 859.196424784. Il envoit un nombre compris entre 1 et 1000 (1000 non compris).
Pour retirer les chiffres après la virgule :

````javascript
console.log(Math.floor(Math.random() * 1000 + 1));
````
La console va renvoyer un nombre aléatoire entre 1 et 1000.

`Math.random` va donc nous permettre d'utiliser des générateurs aléatoires.
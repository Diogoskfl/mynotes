# ReactJs

### Présentation

ReactJS est une bibliothèque JavaScript libre développée par Facebook, dont le but principal est de faciliter la création d'application monopage (Appelées SPA, Single Page App), via la création de composants dépendant d'un état et générant une page (ou portion) HTML à chaque changement d'état. Cette bibliothèque se démarque par sa flexibilité et ses performances, en travaillant avec un DOM virtuel et en ne mettant à jour le rendu dans le navigateur qu'en cas de nécessité.

### Le JSX

````jsx
const element = <h1>Bonjour, monde !</h1>;
````

Cette drôle de syntaxe n’est ni une chaîne de caractères ni du HTML. Ça s’appelle du JSX !
C’est une extension syntaxique de JavaScript. 
Il est recommander d’utiliser du JSX avec React afin de décrire à quoi devrait ressembler l’interface utilisateur (UI). 
Le JSX faitvpenser à un langage de balisage, mais il recèle toute la puissance de JavaScript.
Il produit des « éléments » React.

### Les interpolations

L'interpolation est un calcul, c'est un outil qui nous permet de demander à React d'interpreter la valeur demandée et de ne pas l'utiliser sous sa forme écrite (en string par exemple).
L'interpolation d'une valeur s'écrit ` {valeur} ` .

Par exemple :

````jsx
const name = 'Valentin D';
const element = <h1>Bonjour, {name}</h1>;
````

L'interpolation de la variable ` {name} ` permet à React de comprendre qu'il faut afficher la valeur de la variable et non un string 'name'.

### Les fragments

En React, il est courant pour un composant de renvoyer plusieurs éléments. Les fragments nous permettent de grouper une liste d’enfants sans ajouter de nœud supplémentaire au DOM.
Un fragment s'écrit avec une balise vide ` <> composant </> `
# React Props & State

### Les props

Les *props* sont des arguments ou propriétés passées aux composants React.
Elles sont comme des arguments de fonction en JavaScript et des attributs en HTML.

Pour passer des accessoires dans un composant, utilisez la même syntaxe que les attributs HTML:

````jsx
const myelement = <User gender="homme" />;

return <h2>Je suis un {this.props.gender}!</h1>
````

Le composant << user >> reçoit la props "gender". Elle sera utilisée dans la seconde ligne.

Il faut savoir que des props sont crées par React à la création d'un composant comme  ` children ` et ` className ` , cette dernière étant le nom qu'a donné React à la propriété "class" en HTML natif.

### Le state


Le state est l'endroit où l'on définit les valeurs de propriété qui appartiennent au composant. 
Il est initialisé dans le constructeur et peut contenir autant de propriétés qu'on le souhaite.

````javascript
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        firstName: 'Valentin',
        lastName: 'Diogo',
    };
  }
}
````

#### setState()

Pour modifier une valeur dans le state, on utilise la méthode ` setState() `.

````javascript
function changeState() {
    setState({
    ...state,
    firstName: 'Mathieu',
    });
  }
````
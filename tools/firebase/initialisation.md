# Firebase
C'est un outil qui permet de créer des applications mobiles et des sites internet en facilitant la gestion des bases de données et de l'hebergement.
## Comment mettre un projet en ligne grace a Firebase ?
1. Aller sur le site de firebase
2. Se connecter
3. Creer un projet
4. Sur Webstorm, ouvrir le terminal
5. Saisir npm install -g firebase-tools
6. Créer un fichier qui sera a la racine du projet (index.html)
7. Saisir firebox hosting dans le terminal
8. S'il faut se connecter, saisir firebase login
9. Se connecter sur la fenêtre qui s'est ouverte
10. Selectionner le projet
11. Saisir ./
12. Répondre N aux deux prochaines questions
13. Rajouter a firebase.json,package.json et package-lock.json
14. Saisir firebase deploy